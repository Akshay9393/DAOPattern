package com.nuance.ps.dao;


import java.util.List;

import com.nuance.ps.model.Books;

public interface BookDao {

    List<Books> getAllBooks();
    Books getBookByIsbn(int isbn);
    void saveBook(Books book);
    void deleteBook(Books book);
}

